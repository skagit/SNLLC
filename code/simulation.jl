using DataFrames, DataArrays
@everywhere begin

### DATA TYPES
type Construction
  # a Construction has a meaning, some original source signal pattern,
  #  and a level or renanalysis
  meaning::Int64
  origin::Int64
  level::Int64
end

type Language
  # a Language is a collection of meanings and constructions
  meanings::Int64
  origins::Int64
  construction_i::Int64
  constructions::Array{Construction,1}
end

type Experience
  agent::Int64
  construction::Int64
end

type Agent
  # an agent is a collection of active and passive Language knowledge,
  #  and Experiences
  # active and passive are meaning => construction maps
  # both key and values are indicies to respective Lanuage components
  #  of the Agent's Network
  active::Dict{Int64, Array{Int64,1}}
  passive::Dict{Int64, Array{Int64,1}}
  experience::Array{Experience, 1}
end

type Network
  agents::Array{Agent, 1}
  connections::Array{Tuple{Int64, Int64}, 1} #(agent_index, agent_index) edges
  language::Language
  knows::Dict{Int64, Array{Int64,1}} #maps agent index to their other connected agents
  label::String #name of network
end

###TYPE CONSTRUCTION
#Connection Functions
function neighbors(v, edges)
  filter(edges) do pair
    (i,j) = pair
    if i == v || j == v
      return true
    else
      return false
    end
  end
end

function connect_random(agents, prob)
  n = length(agents)
  complete = [ (i, j) for i in 1:n for j in i+1:n ]
  minimal = [ (i, rand(i+1:n)) for i in 1:n-1 ]
  reduced = filter(complete) do _
    if rand() <= prob
      return true
    else
      return false
    end
  end
  return union(minimal, reduced)
end

function connect_h5n2(agents)
  base = append!([ (1,2) , (2,3) , (3,4) , (4,1) ] , [(i, 5) for i in 1:4])
  externs = [(x+(5*i), y+(5*i)) for i in 1:4 for (x, y) in base]
  centrals = [(x + (5*i), 5) for x in 1:4 for i in 1:4]
  return append!(append!(base, externs), centrals)
end

function connect_h5n3(agents)
  h5n2s = [connect_h5n2(agents) for i in 1:5]
  for i in 2:5
    for (j, (a, b)) in enumerate(h5n2s[i])
      h5n2s[i][j] = (a+(25*(i-1)) , b+(25*(i-1)))
    end
  end
  results = []
  for level in h5n2s
    append!(results, level)
  end
  for i in 30:50
    if i%5 !== 0
      push!(results, (i, 5) )
    end
  end
  for i in 55:75
    if i%5 !== 0
      push!(results, (i, 5) )
    end
  end
  for i in 80:100
    if i%5 !== 0
      push!(results, (i, 5) )
    end
  end
  for i in 105:125
    if i%5 !== 0
      push!(results, (i, 5) )
    end
  end
results
end

function connect_ba(agents)
  function assoc(array, index)
    for (i, val) in array
      if i == index
        return (i, val)
      end
    end
    return "error"
  end

  m = 1
  k = 2 + ((m-2) * 2) #links if initial nodes are connected linearly
  connections = []
  degrees = zeros(Int64, m)
  for i in 1:m-1
    push!(connections, (i, i+1))
    degrees[i] += 1
    degrees[i+1] += 1
  end

  for new_node in m+1:length(agents)
    prob = degrees ./ k
    ids = []
    for i in 1:length(prob)
      push!(ids, (i, prob[i]))
    end

    for c in 1:m
      sort!(ids, by=x->begin (a, b) = x; b end, rev=true)
      id, rem = assoc(ids, prob_pick_id(ids))
      push!(connections, (new_node, id))
      degrees[id] += 1
      ids = [ (index, prob) for (index, prob) in ids if index != id]
      ids = [ (id, prob + (rem / length(ids))) for (id, prob) in ids ]
    end

    push!(degrees, m)
    k += (m * 2)
  end
  connections
end

#Constructors
function prob_pick_id(array)
  target = rand()
  sum = 0
  for (i, prob) in array
    sum += prob
    if target <= sum
      return i
    end
  end
  return array[length(array)][1]
end

function novel_construction(language, meaning) #operates at Language level
  language.origins += 1
  language.construction_i += 1
  novel = Construction(meaning, language.origins, 0)
  push!(language.constructions, novel)
  return language.construction_i #index of added construction
end

function evolve_construction(language, construction)
  old = language.constructions[construction]
  evolved = Construction(old.meaning, old.origin, old.level + 1)
  language.construction_i += 1
  push!(language.constructions, evolved)
  return language.construction_i
end

function new_language(n_meanings)
  meanings = n_meanings
  origins = n_meanings
  construction_i = n_meanings
  constructions = [Construction(m,o,0) for (m,o) in zip(1:meanings, 1:origins) ]
  return Language(meanings, origins, construction_i, constructions)
end

function new_network(n_agents, n_meanings, connection_fn, label)
  language = new_language(n_meanings)
  base_map = Dict(k => [v] for (k,v) in zip(1:n_meanings, 1:n_meanings))
  agents = [Agent(base_map, base_map, []) for _ in 1:n_agents]
  connections = connection_fn(agents)
  knows = Dict(k => [] for k in 1:n_agents)
  for v in 1:n_agents
    partners = map(neighbors(v, connections)) do pair
      (i,j) = pair
      if i == v
        return j
      else
        return i
      end
    end
    for partner in partners
      push!(knows[v], partner)
    end
  end
  return Network(agents, connections, language, knows, label)
end

###METHODS
#Network
function step_network!(network)
  function converse(i, j)
    function canunderstand(target, hearer)
      knowns = hearer.passive[target.meaning]
      if target.level == 0 #paraphrastic phrases are always understood
        return true
      end
      for known in [network.language.constructions[index] for index in knowns]
        #phonetics share same origin, and changes not great, then understood
        if known.origin == target.origin && abs(known.level - target.level) <= 1
          return true
        end
      end
      return false
    end

    function repair(constructions, hearer)
      for index in constructions
        target = network.language.constructions[index]
        understood = canunderstand(target, hearer)
        if understood
          return (true, index)
        end
      end
      return (false, 0)
    end

    function talkto(speaker, hearer)
      meaning = rand(1:network.language.meanings)
      spoken_i = rand(speaker.active[meaning])
      spoken = network.language.constructions[spoken_i]
      understood = canunderstand(spoken, hearer)
      learned = false
      bridge = 0
      if understood
        push!(hearer.experience, Experience(i, spoken_i)) #hearer knows speaker uses this construction
        if !(spoken_i in hearer.passive[meaning]) #hearer updates passive knowledge
          push!(hearer.passive[meaning], spoken_i)
        end
        if rand([true,false,false,false]) #one in four chance
          if !(spoken_i in hearer.active[meaning]) #to update active knowledge
            push!(hearer.active[meaning], spoken_i)
          end
        end
      else
        (learned, bridge) = repair(speaker.active[meaning], hearer) #if not understood, speaker can try to rephrase
      end
      if learned
        push!(hearer.experience, Experience(i, spoken_i)) #hearer knows speaker uses this construction
        #push!(hearer.experience, Experience(i, bridge)) #hearer knows speaker uses this bridge construction
        push!(hearer.passive[meaning], spoken_i) #become able to understand target
        # if !(bridge in hearer.passive[meaning]) #hearer updates passive knowledge with bridge
        #   push!(hearer.passive[meaning], bridge)
        # end
      end
      if !(learned) && !(understood)
        invented = novel_construction(network.language, meaning) #returns index of new construction in language
        #add the novel construction to both active and passive knowledge of speaker and hearer
        push!(speaker.active[meaning], invented)
        push!(speaker.passive[meaning], invented)
        push!(hearer.active[meaning], invented)
        push!(hearer.passive[meaning], invented)
        #speaker and hearer experience eachother using the constuction
        push!(speaker.experience, Experience(j, invented))
        push!(hearer.experience, Experience(i, invented))
      end
    end

    agent_a = network.agents[i]
    agent_b = network.agents[j]
    talkto(agent_a, agent_b)
    talkto(agent_b, agent_a)
  end

  function select_events()
    return shuffle(network.connections)
    #return shuffle!([(v, rand(network.knows[v])) for v in 1:length(network.agents)])
  end
  events = select_events()
  for event = events
    (i,j) = event
    converse(i,j)
  end
end

function evolve_network!(network)
  function evolve_agent(i, agent)
    function select_active(constructions, n_connections)
      candidates = unique(constructions)
      stats = map(candidates) do candidate
        count = length(filter(constructions) do construction
          construction == candidate
        end)
        (network.language.constructions[candidate].level, count, candidate)
      end
      sort!(stats, by = item -> begin
        (level, count, candidate) = item
        count
        end,
        rev=true)
      high = stats[1][2]
      threshed = filter(stats) do item
        (level, count, candidate) = item
        count == high
      end
      sort!(threshed, by = item -> begin
        (level, count, candidate) = item
        level
        end,
        rev=true)
      return shuffle(threshed)[1][3]
    end

    function select_actives(experiences)
      selected = Dict( k => [] for k in 1:network.language.meanings)
      candidates = Dict( k => [] for k in 1:network.language.meanings)
      n_connections = length(network.knows[i])
      for experience in experiences
        construction = experience.construction
        meaning = network.language.constructions[construction].meaning
        push!(candidates[meaning], construction)
      end
      for (meaning, actives) in candidates
        if length(actives) < 1
          push!(candidates[meaning], rand(agent.active[meaning]) )
        end
      end
      for meaning in 1:network.language.meanings
        push!(selected[meaning], select_active(candidates[meaning], n_connections))
      end
      return selected
    end

    function grammaticalize(next_actives, grammar_constant = .01)
      selected = Dict( k => [] for k in 1:network.language.meanings)
      for (meaning, actives) in next_actives
        active = actives[1]
        if network.language.constructions[active].level >= 20 && rand([false, false, true]) #check if too far leveled
          next = novel_construction(network.language, meaning)
          push!(selected[meaning], next)
          continue
        elseif network.language.constructions[active].level < 20 && rand() <= ( length(network.knows[i]) * grammar_constant )
          next = evolve_construction(network.language, active)
          push!(selected[meaning], next)
          continue
        else
        push!(selected[meaning], active)
        end
      end
      return selected
    end

    function select_passives(experiences, next_actives)
      selected = Dict( k => [] for k in 1:network.language.meanings)
      for experience in experiences
        construction = experience.construction
        meaning = network.language.constructions[construction].meaning
        push!(selected[meaning], construction)
      end
      for (meaning, actives) in next_actives
        for active in actives
          if !(active in selected[meaning])
            push!(selected[meaning], active)
          end
        end
      end
      return selected
    end

    exp_types = unique(agent.experience) do experience #return unique experiences, i.e. ignore how many times the same speaker used the same token
      (experience.agent, experience.construction)
    end
    next_actives = select_actives(exp_types) #pick next actives from experience
    grammaticalized = grammaticalize(next_actives) #return new possibly grammaticalized constructions
    next_passives = select_passives(exp_types, grammaticalized) #build passive knowledge from experienced constructions and grammaticalized actives
    return Agent(grammaticalized, next_passives, [])
  end

  for (i, agent) in enumerate(network.agents)
    next_agent = evolve_agent(i, agent)
    network.agents[i] = next_agent
  end
end

#function new_network(n_agents, n_meanings, connection_fn, label)
function run_network(n_agents, n_gens, n_cycles, n_meanings, connection_fn, label)
  net = new_network(n_agents, n_meanings, connection_fn, label)
  snapshots = []
  push!(snapshots, deepcopy(net))
  for gen in 1:n_gens
    for round in 1:n_cycles
      step_network!(net)
    end
    evolve_network!(net)
    push!(snapshots, deepcopy(net))
    println("$(label)_$(gen)")
  end
  snapshots
end

#@everywhere

# DATA COLLECTION

# function create_dataframe(cols)
#   data = DataFrame()
#   for col in cols
#     data[col] = []
#   end
#   data
# end
#
# function update_dataframe(df, networks, num)
#   for (gen, net) in enumerate(networks)
#     for (agent_id, agent) in enumerate(net.agents)
#       for meaning in 1:net.language.meanings
#         for construction in agent.active[meaning]
#           level = net.language.constructions[construction].level
#           origin = net.language.constructions[construction].origin
#           size = 25
#           if net.label[end] == 'x'
#             size = 125
#           end
#           push!(df, @data([num, net.label, size, gen, agent_id, meaning, construction, "$(origin)", level]))
#         end
#       end
#     end
#   end
# end

function update_csv(of, networks, num)

  for (gen, net) in enumerate(networks)
    outstr = ""
    for (agent_id, agent) in enumerate(net.agents)
      for meaning in 1:net.language.meanings
        for construction in agent.active[meaning]
          level = net.language.constructions[construction].level
          origin = net.language.constructions[construction].origin
          size = 25
          if net.label[end] == 'x'
            size = 125
          end
          outstr *= "$(num), $(net.label), $(size), $(gen), $(agent_id), $(meaning), $(construction), $(origin), $(level)\n"
        end
      end
    end
    write(of, outstr)
  end
end

end

### MAIN ###
function experiment1()
  # cols = [:id, :typo, :size, :gen, :agent, :cons, :var, :signal, :level]
  # df = create_dataframe(cols)
  # ed = create_dataframe([:id, :type, :a, :b])

  cols = "id, typo, size, gen, agent, cons, var, signal, level\n"
  outfile = open("/home/matt/Desktop/test.csv", "w")
  write(outfile, cols)

  cols_ed = "id, type, a, b\n"
  outfile_ed = open("/home/matt/Desktop/test_ed.csv", "w")
  write(outfile_ed, cols_ed)
  trials = 2
  round = 0

  ##Experiment one
for r in 1:15
  println("Round~~~$r~~~")

  results = pmap(1:trials) do num
    run_network(125, 500, 10, 3, x->connect_random(x, 1), "completex")
  end

  for (num, res) in enumerate(results)
      edstr = ""
      num += (r - 1)
      update_csv(outfile, res, num)
      for con in res[1].connections
        edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
      end
      write(outfile_ed,  edstr)
  end

  results = pmap(1:trials) do num
    run_network(125, 500, 10, 3, x->connect_random(x, log(125)/125), "giantx")
  end

  for (num, res) in enumerate(results)
      edstr = ""
      num += (r - 1)
      update_csv(outfile, res, num)
      for con in res[1].connections
        edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
      end
      write(outfile_ed,  edstr)
  end

  results = pmap(1:trials) do num
    run_network(125, 500, 10, 3, x->connect_h5n3(x), "hierarchicalx")
  end

  for (num, res) in enumerate(results)
      edstr = ""
      num += (r - 1)
      update_csv(outfile, res, num)
      for con in res[1].connections
        edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
      end
      write(outfile_ed,  edstr)
  end

  results = pmap(1:trials) do num
    run_network(125, 500, 10, 3, x->connect_ba(x), "barabasix")
  end

  for (num, res) in enumerate(results)
      edstr = ""
      num += (r - 1)
      update_csv(outfile, res, num)
      for con in res[1].connections
        edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
      end
      write(outfile_ed,  edstr)
  end

 end

  close(outfile)
  close(outfile_ed)

  # for num in 1:100
  #   println("~~~~~~~~~~$(num)~~~~~~~~~~")
  #   # complete = @spawn run_network(25, 1000, 10, 3, x-> connect_random(x, 1), "complete")
  #   # giant = @spawn run_network(25, 1000, 10, 3, x-> connect_random(x, log(25)/25), "giant")
  #   # h5n2 = @spawn run_network(25, 1000, 10, 3, x-> connect_h5n2(x), "hierarchical")
  #   # ba = @spawn run_network(25, 1000, 10, 3, x->connect_ba(x), "barabasi")
  #   completex = @spawn run_network(125, 1000, 10, 3, x->connect_random(x, 1), "completex")
  #   giantx = @spawn  run_network(125, 1000, 10, 3, x->connect_random(x, log(125)/125), "giantx")
  #   h5n3 =  @spawn run_network(125, 1000, 10, 3, x->connect_h5n3(x), "hierarchicalx")
  #   bax = @spawn run_network(125, 1000, 10, 3, x->connect_ba(x), "barabasix")
  #
  #   results = [fetch(completex), fetch(h5n3), fetch(giantx), fetch(bax)]
  #   #results = [fetch(ba),  fetch(complete), fetch(giant), fetch(h5n2)]
  #
  #   for res in results
  #     edstr = ""
  #     #update_dataframe(df, res, num)
  #     update_csv(outfile, res, num)
  #     for con in res[1].connections
  #       #push!(ed, @data( [num, res[1].label, con[1], con[2]]))
  #       edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
  #     end
  #     write(outfile_ed,  edstr)
  #   end
  #
  #   close(outfile)
  #   close(outfile_ed)
  # end

  # writetable("/home/matt/Desktop/rewrite_df_ex1X.csv", df)
  # writetable("/home/matt/Desktop/rewrite_ed_ex1X.csv", ed)
end

function experiment2()

cols = "id, typo, size, gen, agent, cons, var, signal, level\n"
outfile = open("/home/matt/Desktop/test_ex2.csv", "w")
write(outfile, cols)

cols_ed = "id, type, a, b\n"
outfile_ed = open("/home/matt/Desktop/test_ed_ex2.csv", "w")
write(outfile_ed, cols_ed)

trials = 2
round = 0

##Experiment two
# for r in 1:15
# println("Round~~~$r~~~")
#
# results = pmap(1:trials) do num
#   run_network(125,500,10,3, (agents)->connect_random(agents, 1.0), "1.0x")
# end
#
# for (num, res) in enumerate(results)
#     edstr = ""
#     num += (r - 1)
#     update_csv(outfile, res, num)
#     for con in res[1].connections
#       edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
#     end
#     write(outfile_ed,  edstr)
# end
#
# results = []
#
# end
#
#
# for r in 1:15
# println("Round~~~$r~~~")
#
# results = pmap(1:trials) do num
#   run_network(125,500,10,3, (agents) -> connect_random(agents, 0.9), "0.9x")
# end
#
# for (num, res) in enumerate(results)
#     edstr = ""
#     num += (r - 1)
#     update_csv(outfile, res, num)
#     for con in res[1].connections
#       edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
#     end
#     write(outfile_ed,  edstr)
# end
#
# results = []
#
# end
#
#
# for r in 1:15
# println("Round~~~$r~~~")
#
# results = pmap(1:trials) do num
#   run_network(125,500,10,3, (agents) -> connect_random(agents, 0.8), "0.8x")
# end
#
# for (num, res) in enumerate(results)
#     edstr = ""
#     num += (r - 1)
#     update_csv(outfile, res, num)
#     for con in res[1].connections
#       edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
#     end
#     write(outfile_ed,  edstr)
# end
#
# results = []
#
# end
#
# for r in 1:15
# println("Round~~~$r~~~")
#
# results = pmap(1:trials) do num
#   run_network(125,500,10,3, (agents) -> connect_random(agents, 0.7), "0.7x")
# end
#
# for (num, res) in enumerate(results)
#     edstr = ""
#     num += (r - 1)
#     update_csv(outfile, res, num)
#     for con in res[1].connections
#       edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
#     end
#     write(outfile_ed,  edstr)
# end
#
# results = []
#
# end
#
# for r in 1:15
# println("Round~~~$r~~~")
#
# results = pmap(1:trials) do num
#   run_network(125,500,10,3, (agents) -> connect_random(agents, 0.6), "0.6x")
# end
#
# for (num, res) in enumerate(results)
#     edstr = ""
#     num += (r - 1)
#     update_csv(outfile, res, num)
#     for con in res[1].connections
#       edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
#     end
#     write(outfile_ed,  edstr)
# end
#
# results = []
#
# end
#
# for r in 1:15
# println("Round~~~$r~~~")
#
# results = pmap(1:trials) do num
#   run_network(125,500,10,3, (agents) -> connect_random(agents, 0.5), "0.5x")
# end
#
# for (num, res) in enumerate(results)
#     edstr = ""
#     num += (r - 1)
#     update_csv(outfile, res, num)
#     for con in res[1].connections
#       edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
#     end
#     write(outfile_ed,  edstr)
# end
#
# results = []
#
# end
#
# for r in 1:15
# println("Round~~~$r~~~")
#
# results = pmap(1:trials) do num
#   run_network(125,500,10,3, (agents) -> connect_random(agents, 0.4), "0.4x")
# end
#
# for (num, res) in enumerate(results)
#     edstr = ""
#     num += (r - 1)
#     update_csv(outfile, res, num)
#     for con in res[1].connections
#       edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
#     end
#     write(outfile_ed,  edstr)
# end
#
# results = []
#
# end

for r in 1:15
println("Round~~~$r~~~")

results = pmap(1:trials) do num
  run_network(125,500,10,3, (agents) -> connect_random(agents, 0.3), "0.3x")
end

for (num, res) in enumerate(results)
    edstr = ""
    num += (r - 1)
    update_csv(outfile, res, num)
    for con in res[1].connections
      edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
    end
    write(outfile_ed,  edstr)
end

results = []

end

for r in 1:15
println("Round~~~$r~~~")

results = pmap(1:trials) do num
  run_network(125,500,10,3, (agents) -> connect_random(agents, 0.2), "0.2x")
end

for (num, res) in enumerate(results)
    edstr = ""
    num += (r - 1)
    update_csv(outfile, res, num)
    for con in res[1].connections
      edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
    end
    write(outfile_ed,  edstr)
end

results = []

end

for r in 1:15
println("Round~~~$r~~~")

results = pmap(1:trials) do num
  run_network(125,500,10,3, (agents) -> connect_random(agents, 0.1), "0.1x")
end

for (num, res) in enumerate(results)
    edstr = ""
    num += (r - 1)
    update_csv(outfile, res, num)
    for con in res[1].connections
      edstr *= "($num), $(res[1].label), $(con[1]), $(con[2])\n"
    end
    write(outfile_ed,  edstr)
end

results = []

end

close(outfile)
close(outfile_ed)

end

#experiment1()
experiment2()
