library(plyr)
library(ggplot2)
library(igraph)
library(data.table)

###EXPERIMENT ONE###
df <- fread("/home/matt/Desktop/rewrite_df_ex1.csv", header=T, sep=",")
df <- fread("/home/matt/Desktop/rewrite_df_ex2.csv", header=T, sep=",")
df$typo <- as.factor(df$typo)
df$id <- as.factor(df$id)
df$gen <-as.numeric(df$gen)
df$agent <-as.numeric(df$agent)
df$cons <-as.numeric(df$cons)
df$var <- as.numeric(df$var)
df$signal <-as.numeric(df$signal)
df$level <-as.numeric(df$level)
complexity <- ddply(df[size==25,], "typo", summarise, complexity = mean(level))
com_gen <- ddply(df[size==25,], c("gen", "typo"), summarise, complexity = mean(level))
qplot(gen, (complexity), color=typo, data=com_gen, geom="line")


df <- fread("/home/matt/Desktop/diss_data/prob/df_cppc_exp1_up_p005.csv", header=T, sep=",")
ed <- fread("/home/matt/Desktop/rewrite_ed_ex1.csv", header=T, sep=",")

df$typo <- as.factor(df$typo)
df$id <- as.factor(df$id)
df$gen <-as.numeric(df$gen)
df$agent <-as.numeric(df$agent)
df$cons <-as.numeric(df$cons)
df$var <- as.numeric(df$var)
df$signal <-as.numeric(df$signal)
df$level <-as.numeric(df$level)

ed$type <- as.factor(ed$type)
ed$id <- as.factor(ed$id)
ed$a <- as.numeric(ed$a)
ed$b <- as.numeric(ed$b)


head(df)
head(ed)

complexity <- ddply(df[size==25,], "typo", summarise, complexity = mean(level))
com_gen <- ddply(df[size==25,], c("gen", "typo"), summarise, complexity = mean(level))

complexityX <- ddply(df[size==125,], "typo", summarise, complexity = mean(level))
com_genX <- ddply(df[size==125,], c("gen", "typo"), summarise, complexity = mean(level))


qplot(gen, (complexity), color=typo, data=com_gen, geom="line")
qplot(gen, (complexity), color=typo, data=com_genX, geom="line")


ggplot(com_gen, aes(x=gen, y=complexity, color=typo)) + geom_point() + geom_line() +
  scale_color_discrete(name  = "Network",
                       breaks=c("complete", "giant", "hierarchical", "barabasi"),
                       labels=c("Complete", "Giant", "Hierarchical", "Barabasi-Albert")) +
  ylab("Mean Level of Reanalysis") + xlab("Generation") + ggtitle("Reanalysis by Typology")

ggplot(com_genX, aes(x=gen, y=complexity, color=typo)) + geom_point() + geom_line() +
  scale_color_discrete(name  = "Network",
                       breaks=c("completex", "randomx", "hierarchicalx", "barabasix"),
                       labels=c("Complete", "Random", "Hierarchical", "Barabasi-Albert")) +
  ylab("Mean Level of Reanalysis") + xlab("Generation") + ggtitle("Reanalysis in 125 Agent Networks")


head(complexity)
head(complexityX)

###EXPERIMENT TWO###
df2 <- fread("/home/matt/Desktop/diss_data/prob/df_cppc_exp2.csv", header=T, sep=",")
ed2 <- fread("/home/matt/Desktop/diss_data/prob/ed_cppc_exp2.csv", header=T, sep=",")

df2$typo <- as.factor(df2$typo)
df2$id <- as.factor(df2$id)
df2$gen <-as.numeric(df2$gen)
df2$agent <-as.numeric(df2$agent)
df2$cons <-as.numeric(df2$cons)
df2$var <- as.numeric(df2$var)
df2$signal <-as.numeric(df2$signal)
df2$level <-as.numeric(df2$level)

ed2$type <- as.factor(ed2$type)
ed2$id <- as.factor(ed2$id)
ed2$a <- as.numeric(ed2$a)
ed2$b <- as.numeric(ed2$b)

head(df)
head(ed)


complexity <- ddply(df2, "typo", summarise, complexity = mean(level))
com_gen <- ddply(df2, c("gen", "typo"), summarise, complexity = mean(level))
com_gen$typo <-as.ordered(com_gen$typo)
head(com_gen)

qplot(typo, complexity, data=complexity, geom=c("point", "line"), title="Average Reanalysis vs. transitivity")

qplot(typo, complexity, data=com_gen, geom="point", title="Average Reanalysis vs. transitivity")

ggplot(com_gen, aes(x=gen, y=complexity, color=typo)) + geom_point() + geom_line() +
  scale_color_discrete(name  = "Connection Probability",
                       breaks=c("1.0", "0.9", "0.8", "0.7", "0.6", "0.5", "0.4", "0.3", "0.2", "0.1"),
                       labels=c("1.0", "0.9", "0.8", "0.7", "0.6", "0.5", "0.4", "0.3", "0.2", "0.1")) +
  ylab("Mean Level of Reanalysis") + xlab("Generation") + ggtitle("Reanalysis by Transitivity")

ggplot(complexity, aes(x=typo, y=complexity)) + geom_point() + geom_line() + ylab("Mean Level of Reanalysis") + xlab("Connection Probability") + ggtitle("Reanalysis vs. Transitivity")

head(complexity)

g_9 <- graph.data.frame(subset(ed, id==4 & type == '0.9', select=c(3,4)), directed = FALSE)

g_7 <- graph.data.frame(subset(ed, id==4 & type == '0.7', select=c(3,4)), directed = FALSE)

g_5 <- graph.data.frame(subset(ed, id==4 & type == '0.5', select=c(3,4)), directed = FALSE)

g_3 <- graph.data.frame(subset(ed, id==4 & type == '0.3', select=c(3,4)), directed = FALSE)

transitivity(g_9)
transitivity(g_7)
transitivity(g_5)
transitivity(g_3)

mean_distance(g_9)
mean_distance(g_7)
mean_distance(g_5)
mean_distance(g_3)


max_levels <- ddply(df, c("typo"), summarise, complexity = mean(table(signal)))
max_levels

mean(table(df$signal))



###GRAPH EXAMPLES###
gr <- graph.data.frame(subset(ed, id==4 & type == 'randomx', select=c(3,4)), directed = FALSE)
gh <- graph.data.frame(subset(ed, id==1 & type=="hierarchical", select=c(3,4)), directed = FALSE)
gb <- graph.data.frame(subset(ed, id==4 & type=="barabasi", select=c(3,4)), directed = FALSE)
gc <- graph.data.frame(subset(ed, id==1 & type=="complete", select=c(3,4)), directed = FALSE)
plot(gh, layout=layout.davidson.harel)
hist(degree(gb))
hist(degree(gr))
hist(degree(gh))

##Diameter

diameter_hierarchical = diameter(gh)
diameter_random = diameter(gr)
diameter_ba = diameter(gb)

#Distance

distance_hierarchical = mean_distance(gh)

mean_random_distance = 0
for (i in 1:40) {
  gr <- graph.data.frame(subset(ed, id==i & type == 'randomx', select=c(3,4)), directed = FALSE)
  mean_random_distance = mean_random_distance + mean_distance(gr)
}
mean_random_distance = mean_random_distance / 40

mean_ba_distance = 0
for (i in 1:40) {
  gr <- graph.data.frame(ed[ed$id==i & ed$type=="barabasix", c(3,4)], directed = FALSE)
  mean_ba_distance = mean_ba_distance + mean_distance(gr)
}
mean_ba_distance = mean_ba_distance / 40

##Transitivity

hierarchical_C = transitivity(gh)

mean_random_C = 0
for (i in 1:40) {
  gr <- graph.data.frame(ed[ed$id==i & ed$type=="giant", c(3,4)], directed = FALSE)
  mean_random_C = mean_random_C + transitivity(gr)
}
mean_random_C = mean_random_C / 40

mean_ba_C = 0
for (i in 1:40) {
  gr <- graph.data.frame(ed[ed$id==i & ed$type=="barabasi", c(3,4)], directed = FALSE)
  mean_ba_C = mean_ba_C + transitivity(gr)
}
mean_ba_C = mean_ba_C / 40
