## Raw Data

The data folder contains the raw data used to report the 25-agent and 125-agent simulation results. Files prefixed with `df` indicate data frames of language generated, and those prefixed with `ed` contain edge data to describe network structure. Experiments one and two are indicated with the suffixes `ex1` and `ex2` respectively.

## Code

The simulation was programmed in `Julia` and run on a multiprocessor, Linux environment. The analysis was done using R on the same machine. The code is partially commented, but not edited at present for exposition. If any question arise, please contact the corresponding author at `matt0020 <at> e <dot> ntu <dot> edu <dot> sg` with the appropriate replacements of `@` and `.` as indicated in brackets.

## Plots

Original plots of the experimental data, as contained and analyzed in the provided files, are available for main results of all experiments.
